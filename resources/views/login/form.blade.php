@extends('../layout')
<div class="body">
<div class="row">
    <div class="col-6 m-auto">
    <div class="card card-info">
        <div class="card-title card-header">JAZDA</div>
    </div>
    <div class="card-body">
        <form method="post" action="/login">
            @csrf
            <div class="form-row">
                <div class="form-label">
                    Login
                </div>
                <div>
                    <input type="email" name="login" class="form-control"/>
                </div>
            </div>
            <div class="form-row">
                <div class="form-label">
                    Password
                </div>
                <div>
                    <input type="password" name="password" class="form-control"/>
                </div>
            </div>
            <input type="submit" class="btn btn-success" value="Zaloguj mnie">
        </form>
    </div>
    </div>
</div>
</div>
